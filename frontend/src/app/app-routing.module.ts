import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [];

//If prod env, would include a 404 page
@NgModule({
  imports: [RouterModule.forRoot([{ path: '', redirectTo: 'collection', pathMatch: 'full' }])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
