import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CollectionListComponent } from './collection-list.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    CollectionListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{ path: 'collection', component: CollectionListComponent }])
  ]
})
export class CollectionModule { }
