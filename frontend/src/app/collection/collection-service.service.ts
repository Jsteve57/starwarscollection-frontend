import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ICharacter } from "../collection/character";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CollectionService {


  private collectionUrl = 'http://localhost:3000/api/swc/v1/getMyStarWarsCollection'

  constructor(private http: HttpClient) { }

  getProducts(): Observable<ICharacter[]> {
    return this.http.get<ICharacter[]>(this.collectionUrl, { withCredentials: true }).pipe(catchError(this.handleError))
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `${err.status}, ${errorMessage}`
    }
    else {
      errorMessage = `${err.status}, ${errorMessage}`
    }
    return throwError(errorMessage);
  }
}
