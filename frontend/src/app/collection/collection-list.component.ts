import { Component, OnInit } from '@angular/core';
import { CollectionService } from './collection-service.service'
import { ICharacter } from './character';

@Component({
  selector: 'app-collection-list',
  templateUrl: './collection-list.component.html',
  styleUrls: ['./collection-list.component.css']
})
export class CollectionListComponent implements OnInit {

  constructor(private productService: CollectionService) {
  }

  p: number = 1;
  pageTitle = `Star Wars Characters`;
  errorMessage = ''

  private _listFilter = '';
  get listFilter() {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this.performFilter(value);
  }

  filteredProducts: ICharacter[] = [];
  products: ICharacter[] = [];

  performFilter(filterBy: string): ICharacter[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.products.filter((product: ICharacter) => {
      return product.name.toLocaleLowerCase().includes(filterBy);
    });
  }

  ngOnInit(): void {
    this.productService.getProducts().subscribe({
      next: products => {
        this.products = products;
        this.filteredProducts = this.products;
      },
      error: err => this.errorMessage = err
    });

  }

}
