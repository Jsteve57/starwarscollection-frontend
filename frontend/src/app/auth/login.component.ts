import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service'
import { AuthService } from './auth.service';


@Component({
  providers: [CookieService],
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  pageTitle = 'Log In';

  constructor(private authService: AuthService, private router: Router) { }

  login(loginForm: NgForm) {
    const userName = loginForm.form.value.userName;
    const password = loginForm.form.value.password;
    this.authService.login(userName, password);
    this.router.navigate(['/collection']);
  }

}