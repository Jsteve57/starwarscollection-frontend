import { Injectable } from '@angular/core';
import { User } from './user';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  constructor(private cookieService: CookieService) { }

  currentUser: User | undefined;

  get isLoggedIn(): boolean {
    return this.cookieService.check('userName');
  }

  login(userName: string, password: string): void {
    this.currentUser = {
      userName: userName,
      password: password,
    };
    this.cookieService.set('userName', this.currentUser.userName);
  }

  logout(): void {
    this.cookieService.delete('userName');
  }

}
